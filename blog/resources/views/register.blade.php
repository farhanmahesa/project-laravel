<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Form Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="first"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="last"> <br><br>
        <label>Gender</label> <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nasionality">
            <option value="Indonesia"> Indonesia</option> <br>
            <option value="Malaysia"> Malaysia</option> <br>
            <option value="Singapura"> Singapura</option> <br>
        </select> <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">  
    </form>
</body>
</html>